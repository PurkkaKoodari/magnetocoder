CFLAGS=-O2 -g
LIBS=-Wl,-rpath,/home/petrus/horstiradio/ffmpeg -L/home/petrus/horstiradio/ffmpeg -lavcodec -lavformat -lavutil -lswresample

magnetocoder: *.c
	gcc ${CFLAGS} -o magnetocoder *.c ${LIBS}

clean:
	rm magnetocoder
