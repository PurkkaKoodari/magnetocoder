#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavcodec/avcodec.h>
#include <libswresample/swresample.h>
#include <FLAC/all.h>

struct output_mode {
    enum AVCodecID codec_id;
    const char *codec_name;
    size_t extradata_size;
    int channels;
    uint64_t channel_layout;
    enum AVSampleFormat sample_fmt;
    int sample_rate;
};

static AVFormatContext *output_format_ctx = NULL;
static AVCodecContext *output_codec_ctx = NULL;

static AVStream *output_stream = NULL;

static AVOutputFormat *output_format = NULL;
static AVCodec *output_codec = NULL;

static AVFrame *input_frame = NULL;
static AVFrame *output_frame = NULL;
static AVPacket input_packet;
static AVPacket output_packet;

static uint64_t total_samples = 0;

static const struct output_mode *mode = NULL;

#define IO_BUFFER_SIZE 65536

#define FLAC_STREAMINFO_SIZE 34

static const struct output_mode mode_flac = {
    AV_CODEC_ID_FLAC,
    "flac",
    FLAC_STREAMINFO_SIZE,
    2,
    AV_CH_LAYOUT_STEREO,
    AV_SAMPLE_FMT_S16,
    44100
};
static const struct output_mode mode_vorbis = {
    AV_CODEC_ID_VORBIS,
    "libvorbis",
    0,
    2,
    AV_CH_LAYOUT_STEREO,
    AV_SAMPLE_FMT_FLTP,
    44100
};

static void print_error(const char *message, int ret);
static void open_output();
static void close_output();
static void encode_frame(AVFrame *output_frame);
static bool convert_frame(SwrContext *resample_ctx, AVFrame *input_frame);
static void transcode_file(const char *filename);

static void print_error(const char *message, int ret) {
    char error_msg[256];
    av_strerror(ret, error_msg, sizeof(error_msg));
    fprintf(stderr, "%s: %s\n", message, error_msg);
}

static void open_output() {
    int ret;
    // initialize packet variables
    av_init_packet(&input_packet);
    av_init_packet(&output_packet);
    // allocate frames
    input_frame = av_frame_alloc();
    output_frame = av_frame_alloc();
    if (!input_frame || !output_frame) {
        fputs("Failed to allocate frame\n", stderr);
        exit(1);
    }
    // find output codec and format
    output_codec = avcodec_find_encoder(mode->codec_id);
    if (!output_codec) {
        fputs("Failed to find output codec\n", stderr);
        exit(1);
    }
    output_format = av_guess_format("ogg", NULL, NULL);
    if (!output_format) {
        fputs("Failed to find output format\n", stderr);
        exit(1);
    }
    // create output format context
    avformat_alloc_output_context2(&output_format_ctx, output_format, NULL, "test.ogg"); // TODO change to stdout
    if (!output_format_ctx) {
        fputs("Failed to create output context\n", stderr);
        exit(1);
    }
    // create stream in output format
    output_stream = avformat_new_stream(output_format_ctx, output_codec);
    if (!output_stream) {
        fputs("Failed to create output stream\n", stderr);
        exit(1);
    }
    // create encoder context
    output_codec_ctx = avcodec_alloc_context3(output_codec);
    if (!output_codec_ctx) {
        fputs("Failed to create encoder context\n", stderr);
        exit(1);
    }
    // set encoder parameters
    output_stream->codecpar->codec_type = AVMEDIA_TYPE_AUDIO;
    output_stream->codecpar->codec_id = mode->codec_id;
    output_stream->codecpar->channels = mode->channels;
    output_stream->codecpar->channel_layout = mode->channel_layout;
    output_stream->codecpar->sample_rate = mode->sample_rate;
    output_stream->codecpar->format = mode->sample_fmt;
    // copy codec parameters to encoder
    if ((ret = avcodec_parameters_to_context(output_codec_ctx, output_stream->codecpar)) < 0) {
        print_error("Failed to copy encoder parameters", ret);
        exit(1);
    }
    // open encoder
    if ((ret = avcodec_open2(output_codec_ctx, output_codec, NULL)) < 0) {
        print_error("Failed to open encoder", ret);
        exit(1);
    }
    // copy codec parameters (extradata) back to codecpar
    if ((ret = avcodec_parameters_from_context(output_stream->codecpar, output_codec_ctx)) < 0) {
        print_error("Failed to copy encoder parameters", ret);
        exit(1);
    }
    // dump format info
    av_dump_format(output_format_ctx, 0, "test.ogg", 1); // TODO change to stdout
    // open output file
    if ((ret = avio_open(&output_format_ctx->pb, "test.ogg", AVIO_FLAG_WRITE)) < 0) { // TODO change to stdout
        print_error("Failed to open output file", ret);
        exit(1);
    }
    // write output header
    if ((ret = avformat_write_header(output_format_ctx, NULL)) < 0) {
        print_error("Failed to write output headers", ret);
        exit(1);
    }
}

static void close_output() {
    // flush encoder
    encode_frame(NULL);
    // finish output and free encoder data
    av_write_trailer(output_format_ctx);
    avcodec_free_context(&output_codec_ctx);
    // close output file
    avio_close(output_format_ctx->pb);
    // close muxer
    avformat_free_context(output_format_ctx);
    // free frames
    av_frame_free(&input_frame);
    av_frame_free(&output_frame);
}

static void transcode_file(const char *filename) {
    AVFormatContext *input_format_ctx = NULL;
    AVCodecContext *input_codec_ctx = NULL;
    SwrContext *resample_ctx;
    
    AVCodec *input_codec = NULL;
    
    size_t input_stream_index;
    int ret;
    // open input file
    if ((ret = avformat_open_input(&input_format_ctx, filename, NULL, NULL)) < 0) {
        print_error("Failed to open input file", ret);
        goto transcode_cleanup;
    }
    // determine stream info from file
    if ((ret = avformat_find_stream_info(input_format_ctx, NULL)) < 0) {
        print_error("Failed to read stream info", ret);
        goto transcode_cleanup;
    }
    // print media info
    av_dump_format(input_format_ctx, 0, filename, false);
    // find a mono/stereo audio stream
    for (input_stream_index = 0; input_stream_index < input_format_ctx->nb_streams; input_stream_index++) {
        AVCodecParameters *par = input_format_ctx->streams[input_stream_index]->codecpar;
        if (par->codec_type != AVMEDIA_TYPE_AUDIO) continue;
        if (par->channels != 1 && par->channels != 2) continue;
        break;
    }
    if (input_stream_index == input_format_ctx->nb_streams) {
        fputs("No mono or stereo audio streams found\n", stderr);
        goto transcode_cleanup;
    }
    // find a decoder for the stream
    input_codec = avcodec_find_decoder(input_format_ctx->streams[input_stream_index]->codecpar->codec_id);
    if (!input_codec) {
        fputs("Failed to find decoder for stream\n", stderr);
        goto transcode_cleanup;
    }
    // create decoder context
    input_codec_ctx = avcodec_alloc_context3(input_codec);
    if (!input_codec_ctx) {
        fputs("Failed to create decoder context\n", stderr);
        goto transcode_cleanup;
    }
    // copy codec parameters to decoder
    if ((ret = avcodec_parameters_to_context(input_codec_ctx, input_format_ctx->streams[input_stream_index]->codecpar)) < 0) {
        print_error("Failed to copy decoder parameters", ret);
        goto transcode_cleanup;
    }
    // open decoder
    if ((ret = avcodec_open2(input_codec_ctx, input_codec, NULL)) < 0) {
        print_error("Failed to open decoder", ret);
        goto transcode_cleanup;
    }
    // allocate resampler
    resample_ctx = swr_alloc_set_opts(NULL,
            mode->channel_layout, mode->sample_fmt, mode->sample_rate,
            input_codec_ctx->channel_layout, input_codec_ctx->sample_fmt, input_codec_ctx->sample_rate,
            0, NULL);
    if (!resample_ctx) {
        fputs("Failed to create resampler\n", stderr);
        goto transcode_cleanup;
    }
    // init resampler
    if ((ret = swr_init(resample_ctx)) < 0) {
        print_error("Failed to initialize resampler", ret);
        goto transcode_cleanup;
    }
    // begin transcoding: read packets from input format
    while ((ret = av_read_frame(input_format_ctx, &input_packet)) >= 0) {
        // send packets to input codec
        if ((ret = avcodec_send_packet(input_codec_ctx, &input_packet)) < 0) {
            print_error("Failed to decode packet", ret);
            goto transcode_cleanup;
        }
        av_packet_unref(&input_packet);
        // read frames from input codec
        while ((ret = avcodec_receive_frame(input_codec_ctx, input_frame)) >= 0) {
            if (!convert_frame(resample_ctx, input_frame)) goto transcode_cleanup;
            av_frame_unref(input_frame);
        }
        if (ret != AVERROR(EAGAIN)) {
            print_error("Failed to decode packet", ret);
            goto transcode_cleanup;
        }
    }
    if (ret != AVERROR_EOF) {
        print_error("Failed to read frame", ret);
        goto transcode_cleanup;
    }
transcode_cleanup:
    swr_free(&resample_ctx);
    avcodec_free_context(&input_codec_ctx);
    avformat_close_input(&input_format_ctx);
}

static bool convert_frame(SwrContext *resample_ctx, AVFrame *input_frame) {
    int ret;
    // resample frames
    if ((ret = swr_convert_frame(resample_ctx, NULL, input_frame)) < 0) {
        print_error("Failed to send frame to resampler", ret);
        return false;
    }
    while (true) {
        // init output frame metadata
        output_frame->channel_layout = mode->channel_layout;
        output_frame->format = mode->sample_fmt;
        output_frame->sample_rate = mode->sample_rate;
        output_frame->pts = total_samples;
        // allocate output frame buffer
        output_frame->nb_samples = output_codec_ctx->frame_size;
        if ((ret = av_frame_get_buffer(output_frame, 0)) < 0) {
            print_error("Failed to allocate frame buffer", ret);
            return false;
        }
        // resample frames
        if ((ret = swr_convert_frame(resample_ctx, output_frame, NULL)) < 0) {
            print_error("Failed to receive frame from resampler", ret);
            return false;
        }
        if (!output_frame->nb_samples) break;
        total_samples += output_frame->nb_samples;
        // send frames to output codec
        encode_frame(output_frame);
    }
    return true;
}

static void encode_frame(AVFrame *output_frame) {
    int ret;
    // send frames to output codec
    if ((ret = avcodec_send_frame(output_codec_ctx, output_frame)) < 0) {
        if (ret != AVERROR_EOF || output_frame) {
            print_error("Failed to encode packet", ret);
            exit(1);
        }
    }
    av_frame_unref(output_frame);
    // read packets from output codec
    while ((ret = avcodec_receive_packet(output_codec_ctx, &output_packet)) >= 0) {
        // write packets to output file
        if ((ret = av_write_frame(output_format_ctx, &output_packet)) < 0) {
            print_error("Failed to write frame", ret);
            exit(1);
        }
        av_packet_unref(&output_packet);
    }
    if (ret != AVERROR(EAGAIN) && (ret != AVERROR_EOF || output_frame)) {
        print_error("Failed to encode packet", ret);
        exit(1);
    }
}

void transcode_loop() {
    char filename_buf[256];
    while (true) {
        // read input file name
        if (!fgets(filename_buf, sizeof(filename_buf), stdin)) break;
        // remove trailing newline
        size_t filename_len = strlen(filename_buf);
        if (filename_buf[filename_len - 1] == '\n') filename_buf[filename_len - 1] = 0;
        
        fprintf(stderr, "Transcoding: %s\n", filename_buf);
        transcode_file(filename_buf);
    }
}

int main(int argc, char const *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <vorbis|flac>\n", argv[0]);
        return 1;
    }
    if (strcmp(argv[1], "vorbis") == 0) {
        mode = &mode_vorbis;
    } else if (strcmp(argv[1], "flac") == 0) {
        mode = &mode_flac;
    } else {
        fprintf(stderr, "Usage: %s <vorbis|flac>\n", argv[0]);
        return 1;
    }

    open_output();
    transcode_loop();
    close_output();

    return 0;
}
